<?php
include 'dbConfig.php';
include 'openDB.php';

	$user_id = $_GET["user_id"];
	$timezone = $_GET["timezone"];
	$send = array();

	$friends = array();
	$query = mysql_query("SELECT * FROM friends WHERE user_id =".$user_id);
	while ($row = mysql_fetch_array($query)) {
		# code...
		$data = array();
		$data["friend_id"] = $row["friend_id"];
		$friends[] = $data;
	}
	$send["friends"] = $friends;

	$query = mysql_query("SELECT COUNT(*) FROM follows WHERE follower_id =".$user_id);
	if ($row = mysql_fetch_array($query)){
		$send["countFollowers"] = $row[0];
	}

	$posts = array();
	$query = mysql_query("SELECT * FROM posts WHERE user_id =".$user_id." ORDER BY date DESC");
	while ($row = mysql_fetch_array($query)){
		$data = array();
		$qur = mysql_query("SELECT * FROM users WHERE id=".$user_id);
		$rrow = mysql_fetch_array($qur);
		$data["id"] = $row["id"];
		$data["post"] = $row["post"];
		// $data["post_image"] = $row["post_image"];
		$data["post_video"] = $row["post_video"];
		$data["post_video_thumb"] = $row["post_video_thumb"];
		
		$date = new DateTime($row["date"], new DateTimeZone(date_default_timezone_get()));
		$date->setTimeZone(new DateTimeZone($timezone));
		$data["date"] = $date->format('Y-m-d H:i:s');

		$qur = mysql_query("SELECT * FROM social WHERE user_id=".$user_id." AND type='Post' AND link_id=".$row["id"]);
		if ($rrow = mysql_fetch_array($qur))
		{
			$data["liked"] = $rrow["liked"];
			$data["comment"] = $rrow["comment"];
		}
		else
		{
			$data["liked"] = 0;
			$data["comment"] = 0;
		}

		$qur = mysql_query("SELECT * FROM social WHERE type='Post' AND liked=1 AND link_id=".$row["id"]);
		$liked_count = 0;
		while ($rrow = mysql_fetch_array($qur)) {
			$blocked = 0;
			$qb = mysql_query("SELECT * FROM blocked WHERE (user_id=".$user_id." AND blocker_id=".$rrow["user_id"].") OR (user_id=".$rrow["user_id"]." AND blocker_id=".$user_id.")");
			while ($rb = mysql_fetch_array($qb)) {
				$blocked = 1;
			}

			if ($blocked == 0) {
				$liked_count += 1;
			}
		}
		$data["liked_count"] = $liked_count;

		$qur = mysql_query("SELECT * FROM comments WHERE type='Post' AND link_id=".$row["id"]);
		$comment_count = 0;
		while ($rrow = mysql_fetch_array($qur)) {
			$blocked = 0;
			$qb = mysql_query("SELECT * FROM blocked WHERE (user_id=".$user_id." AND blocker_id=".$rrow["user_id"].") OR (user_id=".$rrow["user_id"]." AND blocker_id=".$user_id.")");
			while ($rb = mysql_fetch_array($qb)) {
				$blocked = 1;
			}

			if ($blocked == 0) {
				$comment_count += 1;
			}
		}
		$data["comment_count"] = $comment_count;
		
		$posts[] = $data;
	}
	$send["posts"] = $posts;

	$result["data"] = $send;
	header('Content-Type: application/json');
	echo json_encode($result);

include 'closeDB.php';
?>