<?php
include 'dbConfig.php';
include 'openDB.php';

   try {
		$server_URL = $_REQUEST['server_URL'];
		$user_id = $_REQUEST['userID'];
		$image_type = $_REQUEST['videoThumbnail'];
		if ($image_type=="true"){
			$location = "PostVideo/";
			$file_ID = $user_id."_".date("Ymd_His");
			$filename = "post_video_".$file_ID."_thumb.jpg";
		}else{
			$location = "PostImage/";
			$filename = "post_photo_".$user_id."_".date("Ymd_His").".jpg";
		}
		        
        $base = $_REQUEST['image'];
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');
        $file = fopen($location.$filename, 'wb');

        // Create File
        fwrite($file, $binary);
        fclose($file);
        
		if ($image_type=="true")
			echo $file_ID;
		else
	        echo $server_URL.$location.$filename;
    } catch (Exception $e) {
        echo "";
    }

include 'closeDB.php';
?>